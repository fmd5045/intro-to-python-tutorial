# Intro to Python Tutorial

Supporting resources for the Intro to Python tutorial on Udemy:

**00_Intro_to_Notebooks.ipynb**
*  This file has a quick overview of how Jupyter Notebooks work.

To access the exercises and complete them, download the Exercises folder and then access via the Jupyter interface.

**Exercise List**
1 - Numbers
2 - Strings
3 - Sequences
4 - Dictionaries
5 - Conditionals
6 - Functions
7 - Loops